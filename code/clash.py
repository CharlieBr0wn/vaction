# coding = gbk
import os
import urllib.request
import re
import time


def clashInput():
    close(25500)
    path = """E:\\code\\v2rayN\\subconverter\\subconverter.exe"""
    f = os.popen(path)
    url = "http://127.0.0.1:25500/sub?target=clash&url=https%3A%2F%2Fgitlab.com%2FCharlieBr0wn%2Fvaction%2F-%2Fraw%2Fmain%2Findex.txt&emoji=true"
    response = urllib.request.urlopen(url)
    clashText = str(response.read().decode('utf-8'))
    with open(r'E:\code\v2rayN\vaction\clash.yml', 'w', encoding='utf-8') as file:
        file.truncate(0)
        file.write(clashText)
    # f.close()
    file.close()
    close(25500)
    os._exit(0)


def close(port):
    ret = os.popen("netstat -nao|findstr " + str(port))
    str_list = ret.read().encode('utf-8').decode('utf-8')
    if 'TCP' in str_list:
        ret_list = str_list.replace(' ', '')
        ret_list = re.split('\n', ret_list)
        listening_list = [rl.split('LISTENING') for rl in ret_list]
        process_pids = [ll[1] for ll in listening_list if len(ll) >= 2]
        process_pid_set = set(process_pids)
        for process_pid in process_pid_set:
            os.popen('taskkill /pid ' + str(process_pid) + ' /F')
    elif 'UDP' in str_list:
        ret_list = re.split(' ', str_list)
        process_pid = ret_list[-1].strip()
        if process_pid:
            os.popen('taskkill /pid ' + str(process_pid) + ' /F')



if __name__ == '__main__':
    clashInput()
